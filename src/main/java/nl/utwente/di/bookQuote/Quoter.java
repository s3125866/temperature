package nl.utwente.di.bookQuote;

public class Quoter {
    double getBookPrice(String temperature){
        double celsius = Double.parseDouble(temperature);
        double fahrenheit = (celsius * 9 / 5) + 32;
        return fahrenheit;
    }
}
